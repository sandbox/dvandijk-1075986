<?php

/**
 * @file
 * This module acts as a performance enhancher on sites
 * with very large menustructures.
 *
 * When detecting a menu larger than the maximun allowable
 * size it truncates the select element and inserts an option
 * for loading the entire tree through AJAX.
 *
 * This wil increase the page load when editting nodes and decreases
 * the need for a lot of memory.
 */

/**
 * Implementation of hook_menu().
 */
function largemenus_menu() {
  $items = array();

  $items['admin/settings/largemenus'] = array(
    'title' => t('Large menus'),
    'description' => t('Performance upgrade for large menus'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('largemenus_settings'),
    'access arguments' => array('administer site configuration'),
  );

  $items['largemenus/load/all/%/%'] = array(
    'title' => '',
    'page callback' => 'largemenus_load_all',
    'access arguments' => array('administer nodes'),
    'page arguments' => array(3, 4),
  );
  return $items;
}

/**
 * Settings for for the module.
 */
function largemenus_settings() {
  $form = array();

  $form['largemenus_max_menu_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum allow menu entries'),
    '#description' => t('Enter a number. Menu with more entries will be shortend.'),
    '#required' => 1,
    '#default_value' => variable_get('largemenus_max_menu_size', 200),
  );

  $form['#submit'][] = 'max_menu_size_is_int';
  return system_settings_form($form);
}

/**
 * Convert to an integer
 */
function max_menu_size_is_int($form, &$form_state) {
  $form_state['values']['max_menu_size'] = abs($form_state['values']['max_menu_size']);
}

/**
 * Print the entire menu tree in a select element
 * @param int $mlid
 * @param string $current
 */
function largemenus_load_all($mlid = 0, $current = '') {
  $item['mlid'] = $mlid;
  if ($options = menu_parent_options(menu_get_menus(), $item)) {
    print '<select id="edit-menu-parent" class="form-select menu-title-select" name="menu[parent]">';
    foreach ($options as $i => $e) {
      print '<option ' . ($i == $current ? 'selected="selected"' : '') . ' value="' . $i . '">' . $e . '</option>';
    }
    print '</select>';
  }
  exit;
}

/**
 * Implementation of hook_form_alter().
 */
function largemenus_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#node']->type . '_node_form' == $form_id) {
    $numberItems = count($form['menu']['parent']['#options']);
    if ($numberItems > variable_get('largemenus_max_menu_size', 200)) {
      drupal_add_js(drupal_get_path('module', 'largemenus') . '/js/largemenus.js');
      drupal_add_css(drupal_get_path('module', 'largemenus') . '/css/largemenus.css');

      $form['menu']['parent']['#options'] = array(
        $form['menu']['parent']['#default_value'] => $form['menu']['parent']['#options'][$form['menu']['parent']['#default_value']],
        'load_all_' . $form['#node']->menu['mlid'] . '_' . $form['#node']->menu['menu_name'] . ':' . $form['#node']->menu['plid'] => t('--- Laadt dit hele menu in (#!count) ---', array('!count' => $numberItems)),
      );
    }
  }
}

